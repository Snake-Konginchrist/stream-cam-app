# Stream Cam

Stream Cam is a Flutter-based mobile application that captures real-time video from the smartphone's camera and streams it directly to a PC within the same local network. This app is ideal for remote monitoring, live broadcasting, and other multimedia applications.

## Features

- **Real-time Video Streaming:** Stream high-quality video from your mobile device to any PC on the same network.
- **Network Discovery:** Automatically detect and connect to a server using local network discovery protocols.
- **Ease of Use:** Simple user interface for starting and stopping the stream with just one tap.
- **Cross-Platform:** Compatible with multiple operating systems for the receiver including Windows, macOS, and Linux.

## Installation

To run Stream Cam on your device, follow these steps:

1. **Clone the Repository:**
   ```bash
   git clone https://github.com/yourusername/stream-cam.git
   cd stream-cam
   ```

2. **Install Dependencies:**
   ```bash
   flutter pub get
   ```

3. **Run the App:**
   ```bash
   flutter run
   ```

Ensure your development environment includes Flutter SDK setup for detailed installation instructions visit [Flutter official setup guide](https://flutter.dev/docs/get-started/install).

## Usage

1. Open the app on your smartphone.
2. Ensure that both your smartphone and the PC are connected to the same local network.
3. Press the 'Start Streaming' button to begin broadcasting video.
4. Open your PC client to receive and display the video.

## Contributing

Contributions are what make the open-source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

## License

Distributed under the MIT License. See `LICENSE` for more information.