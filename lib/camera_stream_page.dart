import 'package:flutter/material.dart';
import 'package:camera/camera.dart';  // 导入camera包，用于控制摄像头
import 'websocket_manager.dart';  // 导入WebSocket管理模块

// 定义摄像头流页面的状态管理类
class CameraStreamPage extends StatefulWidget {
  const CameraStreamPage({super.key});

  @override
  _CameraStreamPageState createState() => _CameraStreamPageState();
}

// 状态管理实现
class _CameraStreamPageState extends State<CameraStreamPage> {
  CameraController? _controller;  // 定义摄像头控制器
  List<CameraDescription>? cameras;  // 可用摄像头列表

  @override
  void initState() {
    super.initState();
    _initializeCamera();  // 初始化摄像头
  }

  // 初始化摄像头函数
  Future<void> _initializeCamera() async {
    cameras = await availableCameras();  // 获取可用摄像头列表
    _controller = CameraController(cameras![0], ResolutionPreset.medium);  // 实例化摄像头控制器
    await _controller!.initialize().then((_) {
      if (!mounted) return;  // 检查Widget是否还在显示
      setState(() {});  // 触发重新构建界面
    });
  }

  @override
  void dispose() {
    _controller?.dispose();  // 释放摄像头控制器资源
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_controller == null || !_controller!.value.isInitialized) {
      return const Center(child: CircularProgressIndicator());  // 显示加载中动画
    }
    return Scaffold(
      appBar: AppBar(title: const Text('Camera Stream')),  // 应用栏标题
      body: Column(
        children: <Widget>[
          Expanded(child: CameraPreview(_controller!)),  // 显示摄像头预览
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: () => WebSocketManager.startStreaming(_controller!),  // 点击按钮开始发送视频流
              child: const Text('Start Streaming'),
            ),
          ),
        ],
      ),
    );
  }
}