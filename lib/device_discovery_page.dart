import 'package:flutter/material.dart';
import 'package:multicast_dns/multicast_dns.dart';

// StatefulWidget 类型的 DeviceDiscoveryPage，用于创建可变状态的组件
class DeviceDiscoveryPage extends StatefulWidget {
  const DeviceDiscoveryPage({super.key});

  @override
  _DeviceDiscoveryPageState createState() => _DeviceDiscoveryPageState();
}

// _DeviceDiscoveryPageState 类管理 DeviceDiscoveryPage 的状态
class _DeviceDiscoveryPageState extends State<DeviceDiscoveryPage> {
  final MDnsClient _mdns = MDnsClient();  // 实例化多播DNS客户端
  final String _serviceType = "_yourapp._tcp.local";  // 定义您的服务类型，这里为您的应用服务
  final Map<String, String> _devices = {};  // 用于存储发现的设备列表，包括设备名和连接状态

  @override
  void initState() {
    super.initState();
    startDiscovery();  // 在初始化时启动服务发现
  }

  // 开始发现服务的方法
  void startDiscovery() async {
    await _mdns.start();  // 启动多播DNS客户端
    // 异步循环查询所有广播的服务指针记录
    await for (PtrResourceRecord ptr in _mdns.lookup<PtrResourceRecord>(
        ResourceRecordQuery.serverPointer(_serviceType))) {
      // 对每个服务指针记录，查询对应的服务资源记录
      await for (SrvResourceRecord srv in _mdns.lookup<SrvResourceRecord>(
          ResourceRecordQuery.service(ptr.domainName))) {
        setState(() {
          _devices['${srv.target}:${srv.port}'] = "未连接";  // 将服务的目标主机名和端口添加到设备列表，初始状态为未连接
        });
      }
    }
  }

  // 尝试连接到设备的方法
  void tryConnect(String device) async {
    // 模拟设备连接过程
    await Future.delayed(const Duration(seconds: 2));  // 模拟网络延时
    if (device.contains("192.168")) {  // 假设成功条件
      setState(() {
        _devices[device] = "已连接";
      });
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text("连接成功"),
          content: Text("已成功连接到设备: $device"),
          actions: <Widget>[
            TextButton(
              child: const Text('确定'),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      );
    } else {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text("连接失败"),
          content: Text("无法连接到设备: $device"),
          actions: <Widget>[
            TextButton(
              child: const Text('重试'),
              onPressed: () {
                Navigator.of(context).pop();
                tryConnect(device);  // 重新尝试连接
              },
            ),
            TextButton(
              child: const Text('取消'),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      );
    }
  }

  @override
  void dispose() {
    _mdns.stop();  // 组件被销毁时停止多播DNS客户端
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Network Devices'),  // 应用栏标题
      ),
      body: ListView.builder(
        itemCount: _devices.length,  // 列表项数等于发现的设备数
        itemBuilder: (context, index) {
          String device = _devices.keys.elementAt(index);
          return ListTile(
            title: Text("$device (${_devices[device]})"),  // 列表项标题显示设备名和连接状态
            onTap: () {
              if (_devices[device] == "未连接") {
                tryConnect(device);  // 点击未连接的设备时尝试连接
              }
            },
          );
        },
      ),
    );
  }
}