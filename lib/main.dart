import 'package:flutter/material.dart';
import 'camera_stream_page.dart';  // 导入摄像头流页面模块
import 'device_discovery_page.dart';  // 导入设备发现页面模块

void main() async {
  runApp(const CameraStreamApp());  // 运行应用
}

// 应用的主框架，使用 StatefulWidget 来允许动态状态更新
class CameraStreamApp extends StatefulWidget {
  const CameraStreamApp({super.key});

  @override
  _CameraStreamAppState createState() => _CameraStreamAppState();  // 创建状态
}

// 主应用的状态类
class _CameraStreamAppState extends State<CameraStreamApp> {
  int _currentIndex = 0;  // 当前选中的页面索引
  final List<Widget> _pages = [
    const CameraStreamPage(),  // 摄像头流页面
    const DeviceDiscoveryPage(),  // 设备发现页面
  ];

  // 底部导航项被点击时的处理函数
  void _onItemTapped(int index) {
    setState(() {
      _currentIndex = index;  // 更新当前选中的索引，触发界面重建
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Camera Stream',  // 应用标题
      theme: ThemeData(
        primarySwatch: Colors.blue,  // 主题颜色为蓝色
        visualDensity: VisualDensity.adaptivePlatformDensity,  // 根据平台调整视觉密度
      ),
      home: Scaffold(
        body: _pages[_currentIndex],  // 主内容为当前选中的页面
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.camera),  // 摄像机图标
              label: 'Camera',  // 标签为“摄像头”
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.device_hub),  // 设备集线器图标
              label: 'Discovery',  // 标签为“发现”
            ),
          ],
          currentIndex: _currentIndex,  // 当前选中的项
          onTap: _onItemTapped,  // 点击时调用的函数
        ),
      ),
    );
  }
}