import 'package:camera/camera.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class WebSocketManager {
  static WebSocketChannel? channel;  // WebSocket通道实例
  static String streamUrl = 'ws://192.168.1.2:3000';  // 服务器的WebSocket地址

  static void setStreamUrl(String newUrl) {
    streamUrl = newUrl;
  }

  // 检查 WebSocket 是否已连接且未关闭
  static bool get isChannelOpen => channel != null && channel!.closeCode == null;

  // 开始视频流传输的静态方法
  static void startStreaming(CameraController controller) {
    if (!isChannelOpen) {
      channel = WebSocketChannel.connect(Uri.parse(streamUrl));  // 连接到WebSocket服务器
    }

    // 开始从摄像头接收图像数据流
    controller.startImageStream((cameraImage) {
      // 如果 WebSocket 通道仍然开放，发送数据
      if (isChannelOpen) {
        final bytes = cameraImage.planes.first.bytes;  // 获取图像数据
        channel!.sink.add(bytes);  // 发送图像数据到WebSocket服务器
      }
    });
  }

  // 停止视频流传输的静态方法
  static void stopStreaming() {
    if (isChannelOpen) {
      channel!.sink.close();  // 关闭WebSocket通道
    }
    channel = null;  // 清除通道对象
  }
}